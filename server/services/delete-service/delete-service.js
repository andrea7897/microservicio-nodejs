const express = require('express');
const router = express.Router();
const Product = require('../model/product');
router.get('/delete/:id', async (req, res, next) => {
    let { id } = req.params;
    await Product.remove({_id: id});
    res.redirect('/');
  });